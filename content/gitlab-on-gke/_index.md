# Introduction to GitLab on GKE


## GSP494

![[/fragments/labmanuallogo]]


## Overview

[Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine/) (GKE) is the first production-ready managed service for running containerized applications and simplifies the management and use of containers at scale. GKE makes spinning up, scaling up and down, and retiring Kubernetes clusters easier than it has ever been. This is helping to bring the use of Kubernetes to run cloud-native apps to a larger number of practitioners.

However, a Kubernetes cluster alone doesn’t make great apps. Developers do! The software that will run in these managed Kubernetes clusters needs to be imagined, created, tested, released, and managed. [GitLab](https://about.gitlab.com/) is the single application that provides all of these capabilities out of the box for a seamless, low maintenance, just-commit-code software development, and delivery experience.

In this lab you will go through the process of creating a GKE cluster, installing [Helm](https://helm.sh/) (the package manager for Kubernetes), installing a GitLab instance into GKE, editing a web app, and then deploying it to production on the GKE cluster.

### What you will build

In this lab, you are going to install GitLab from scratch into a GKE cluster, then configure and use GitLab to edit and deploy a web app to production. This lab will illustrate how easy it is to get an entire high-quality DevOps software delivery pipeline working, without the hassles of integrations, so that you can stay focused on creating great software.

### What you'll learn

In this lab you'll learn how to do the following:

* Create, connect to, and configure a Google Kubernetes Engine (GKE) Cluster
* Collect needed information about GKE cluster details
* Install the Helm package manager into the GKE Cluster
* Install a GitLab instance into the GKE cluster
* Clone a simple “Hello World”-style web app project into GitLab from a third party code repository.
* Configure the GitLab instance for Kubernetes and Auto DevOps.
* Make an edit to the web app and redeploy into production.
* Observe how much of the software delivery life cycle GitLab does for you, out of the box.

<!-- ### Prerequisites

This is an __advanced level__ lab. This assumes some familiarity with GKE. Proficiency with shell environments and GCP Console is required. If you need to brush up on these skills, please take one of the following labs before attempting this one:

* [Hello Node Kubernetes](https://google.qwiklabs.com/labs/468/)

Once you're ready, scroll down and follow the steps below to get your lab environment set up. -->

### What you'll need

* A recent version of [Chrome](https://www.google.com/chrome/) is recommended.
* Basic knowledge of Linux CLI and [gcloud](https://cloud.google.com/sdk/gcloud/).

This lab is focused on GKE and GitLab deployment and management. Non-relevant concepts and code blocks are glossed over and are provided for you to simply copy and paste.

Since you will use a generated Google lab account it is recommended that you run this lab in your Chrome browser's __incognito mode__, to isolate cookies from your regular browser usage.

## Setup

![[/fragments/startqwiklab]]

![[/fragments/gcpconsole]]

![[/fragments/cloudshell]]

## Picking a Zone

To spread the load amongst the different zones within the specific region where this Qwiklab is running, execute the following command within Cloud Shell:
```
python -c 'import random, os; l = random.choice(["a", "b", "c", "f"]); os.system("gcloud config set compute/zone us-central1-{}".format(l)); print("You will be using GCP zone: us-central1-{}".format(l))'
```

This command will randomly select a GCP Compute zone within the `us-central1` region, set it as the default zone for your Cloud Shell session, and display the zone within the Cloud Shell console for your reference. You should receive a similar output:

```output
Updated property [compute/zone].
You will be using GCP zone: us-central1-a
```
<aside>A full list of available GCP zones can be found <a href="https://cloud.google.com/compute/docs/regions-zones/"  target="_blank">here</a>.</aside>

### Expand Cloud Shell

To make it easier to work with the info from Cloud Shell, expand it to a larger view port. You can do this by clicking on the expand icon in the upper right of the terminal:

![expand cloud shell window](img/setup/expand_cloudshell.png)

## Create a Google Kubernetes Engine (GKE) Cluster

In this section you will create a Kubernetes cluster on GKE which you will later install GitLab into, as well as deploy managed apps to.

### Create the GKE cluster
In the Cloud Shell command line, create a 3 node GKE cluster by executing the following command:

```
gcloud container clusters create gitlab-gke-cl --num-nodes=3 --machine-type=n1-standard-4 --disk-type "pd-ssd" --disk-size "100"
```
The output from this command will display several warnings which you can ignore. It may take a few minutes to complete.

Once the GKE cluster has been created, you should see output similar to this:

```output
NAME           LOCATION       MASTER_VERSION  MASTER_IP        ...
gitlab-gke-cl  us-central1-b  1.11.7-gke.4    35.225.100.135   ...
```

### Get authentication credentials for the cluster

Next, you will need authentication credentials to interact with the cluster.

To get authentication credentials for the GKE cluster run the following command:

```
gcloud container clusters get-credentials gitlab-gke-cl
```

Example output:

```output
Fetching cluster endpoint and auth data.
kubeconfig entry generated for gitlab-gke-cl.
```

## Install Helm

Helm is a Kubernetes package manager that helps you manage Kubernetes applications. Helm is made up of two parts: The Helm client (helm) and the Helm server (Tiller). Packages within Helm are called Charts. In this lab, you will use the GitLab Helm chart to install GitLab onto your GKE cluster.

First, run the following command to download and install the Helm client:

```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
```

Next, in order to give Tiller the permissions it will need to run on the cluster, you will make a `serviceaccount` for it:

```
kubectl create serviceaccount -n kube-system tiller
```

Now bind the `serviceaccount` to the **cluster-admin** role:

```
kubectl create clusterrolebinding tiller-binding --clusterrole=cluster-admin --serviceaccount kube-system:tiller
```

You can now init Helm using the new privileged `serviceaccount`
```
helm init --service-account tiller
```

Expected output on success:

![helm_init_success](img/setup/helm_init_success.png)

Finally, add the official GitLab helm repo:
```
helm repo add gitlab https://charts.gitlab.io/
helm repo update
```

Expected output on success:

```output
Update Complete. ⎈ Happy Helming!⎈
```

## Get Your Inputs Ready

Before you can deploy GitLab to your GKE cluster, you will need a few pieces of information to plug in to the helm installation command.

### Allocate a Static IP address

Allocate a static IP in the GCP region that your GKE cluster resides in using the following command:
```
gcloud compute addresses create endpoints-ip --region us-central1
```

Verify and store this IP address in an environment variable to be used in the next step. The below command will also output your assigned static IP address.

```
export LB_IP_ADDR="$(gcloud compute addresses list | awk '/endpoints-ip/ {print$2}')"; echo "LB_IP_ADDR=${LB_IP_ADDR}"
```

To ensure that this IP address is propagated by nip.io DNS before going on (otherwise you will have certificate creation issues) execute the following command:

```
nslookup ${LB_IP_ADDR}.nip.io
```

If DNS has not propagated yet, the command will return something similar to:

```output
** server can't find 35.188.126.244.nip.io: NXDOMAIN
```

Keep executing the above 'nslookup' command until it comes back with something like:

```output
Non-authoritative answer:
Name:   35.188.126.244.nip.io
Address: 35.188.126.244
```

### Set your email address

A valid email address is needed so that [Let's Encrypt](https://letsencrypt.org/) can create a proper certificate. If it can't then you won't even be able to login for the first time. Run the following command to set the `EMAIL` variable to the username found in the Connection Details panel:

```
export EMAIL="$(gcloud auth list 2> /dev/null | awk '/^\*/ {print $2}')"; echo $EMAIL
```

## Deploy GitLab to the GKE Cluster

### Run the Helm command to start the installation

Now you are ready to deploy GitLab to your GKE cluster leveraging the GitLab Helm chart. To do so, run the
following command:
```
helm upgrade --install gitlab gitlab/gitlab                    \
             --timeout 600                                     \
             --set global.hosts.externalIP=${LB_IP_ADDR}       \
             --set global.hosts.domain=${LB_IP_ADDR}.nip.io    \
             --set gitlab-runner.runners.privileged=true       \
             --set certmanager-issuer.email=${EMAIL}
```
This will install GitLab from the official Helm chart, using the configuration
values you provided.

### Validate GitLab Installation

In roughly a minute, the deployment of your GitLab instance into your GKE cluster should begin. You can see everything that is being deployed by running the following command:

```bash
kubectl get pods
```

The output should look something like:

![get pods output](img/create_cluster/output_get_pods.png)

To see when GitLab is ready for you to login, run the following command:
```
watch kubectl get pods -l app=unicorn
```

You can log-in to the GitLab web interface once the output shows at least 1 `gitlab-unicorn` is available in the READY column. For example:

```output
NAME                              READY     STATUS    RESTARTS   AGE
gitlab-unicorn-85747856b8-qt7bq   2/2       Running   0          4m
gitlab-unicorn-85747856b8-xt4qf   2/2       Running   0          3m
```

This can take several minutes. Once you see this, type `CTRL + C` to return to the interactive shell.

## Login and Configure GitLab

### Initial Login

Run the following command in the Cloud Shell to generate your new GitLab instance's URL:
```
echo; echo "https://gitlab.${LB_IP_ADDR}.nip.io"; echo
```

Then, in the Cloud Shell, click on the resulting URL to open a browser window/tab on your new GitLab instance.

![gitlab url](img/login/gitlab_url.png)

This should bring you to the GitLab login screen.

![login screen](img/login/login_screen.png)

Login with the username `root`.

You will need to obtain the root password of your GitLab instance by running the following command:
```
echo; kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath={.data.password} | base64 --decode ; echo; echo
```

Copy the output of this command (make sure to get the whole thing), and paste into the password field.

### Add a License

Since you deployed the Enterprise Edition of GitLab, you can check out some of the more advanced features if you add a license (otherwise it will behave just like the Community Edition.)

Click on the __wrench icon__ towards the end of the top menu bar (if you don't see the __wrench icon__ make sure your window is wide enough, or look under the __More__ menu):

![wrench icon](img/login/wrench_icon.png)

This will bring you to the admin area. Click on __License__ in the left-hand sidebar:

![license icon](img/login/license_menu.png)

Click on the __Upload New License__ button in the top right corner:

![upload license button](img/login/upload_license_button.png)

Ensure the radio button next to `Enter License Key` is selected:

![license config](img/login/license_config.png)

Then from Cloud Shell, run the following command to output your trial license, and copy that license to the `License Key` text field:

```
echo; curl https://gitlab.com/gitlab-workshops/gitlab-on-gke/raw/master/gitlab-on-gke/gitlab.license; echo; echo
```

Click the blue __Upload License__ button:

![license config](img/login/license_submit.png)

If it worked you should see the following message:

![license success](img/login/license_success.png)

That's it! Go back to the home screen by clicking on the GitLab logo in the top left corner of the window.

![click to home](img/login/click_to_home.png)

### Create a New User

Click on the __Add people__ rectangle on the GitLab home screen.

![add people](img/login/add_people.png)

Here, you will fill out the information for your new account on your instance. The user should be fine with the `Regular` access level.

![user details](img/login/user_details.png)

When you are done, click the green __Create user__ button at the bottom of the screen.

## Setup Your Project

### Impersonate Your New User

Click on the __Impersonate__ button at the top of your new user's page:

![Impersonate](img/login/impersonate.png)

Now you are masquerading as your new user! The reason for doing this is so that we can avoid having to define a password for this user to streamline the steps within this lab.

### Migrate Repo from GitHub to GitLab

You can migrate your repositories from GitHub to GitLab using the `GitHub` importer, which will migrate the whole project including the repository, its branches, commit history, issues, pull requests, pages, labels, milestones, etc. See the <a href="https://docs.gitlab.com/ee/user/project/import/github.html" target="_blank">GitHub Importer page</a> for more details.

You can also just import any git repository by URL by using the `Repo by URL` importer. This imports the repository and its change history, branches, tags, etc. This is the method you'll use for this lab.

Click on the __Create a project__ rectangle.

![create project](img/migrate/create_project.png)

Click on the __Import Project__ tab, then click __Repo by URL__.

![import project](img/migrate/import_project.png)

Enter the following URL into the `Git Repository URL` text field:

```
https://github.com/gitlab-demo/spring-app.git
```

Ensure the `Project slug` text field reads `spring-app`. Also ensure the `Visibility Level` of the repo is set to `Public`. Finally, click the green __Create Project__ button to start the migration process.

![repo by url](img/migrate/repo_by_url.png)

### Add Kubernetes Cluster Info

Next you will connect a Kubernetes cluster to your project so that GitLab can automatically manage the cluster to provide needed resources as you work on your project.

When the migration process finishes, you should be redirected to your new project's page. Click on the __Add Kubernetes cluster__ button near the top.

![add k8s cluster](img/k8s_cluster/add_k8s_cluster.png)

If your GitLab instance is set up to use Oauth authentication with Google then you can have GitLab create a cluster and connect to it for you. It would have looked like this:

![gke with oath set up](img/k8s_cluster/gke_with_oauth_set_up.png)

__However__ you are NOT going to set up your instance with Oauth for this lab, __and__ will re-use the same cluster that you just installed GitLab on (normally, you would set up a separate cluster to install your apps into).

So instead, click on the __Add existing cluster__ tab:

![add existing cluster](img/k8s_cluster/k8s_add_existing.png)

Next, you will need to provide GitLab with some information about your Kubernetes cluster in order to hook it up to your repo and install some software.

#### Name and API URL

The `Kubernetes Cluster Name` can be set to whatever you like (i.e. my-gke-cl).

To get the `API URL`, run the following command in Cloud Shell:

```
echo; kubectl cluster-info | awk '/Kubernetes master/ {print $NF}'; echo
```
Add the resulting URL to the `API URL` field on the Kubernetes details page:

![k8s name and url](img/k8s_cluster/k8s_add_name_and_url.png)

#### CA Certificate

Get your cluster's CA Certificate by running the following in the Cloud Shell:

```
echo; kubectl get secret $(kubectl get secrets | grep default-token| awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode; echo; echo
```

Copy the certificate from your terminal and paste it into the `CA Certificate`
field. Make sure to copy both the `---BEGIN CERTIFICATE---` and the `---END CERTIFICATE---` lines as well.

![k8s add cert](img/k8s_cluster/k8s_add_cert.png)

#### Service Account Token

To give the GitLab associated Tiller (server componment of Helm) the permissions it will need to run on the cluster, you will make a `serviceaccount` for it. Do this by running the following command in Cloud Shell:

```
kubectl create serviceaccount -n default gitlab
```

Next, bind the `serviceaccount` to the cluster-admin role by running:

```
kubectl create clusterrolebinding gitlab-cluster-admin --serviceaccount default:gitlab --clusterrole=cluster-admin
```

Now you can get the service token to share to GitLab. Do this by running the following command:
```
echo; kubectl -n default get secret $(kubectl -n default get secrets| awk '/^gitlab-token/ {print $1}') -o jsonpath="{['data']['token']}" | base64 --decode; echo; echo
```

Copy the resulting token string and paste it into the GitLab Kubernetes setup form under `Token`. __BE CAREFUL__. Copying from the cloud shell inserts spaces at the lines breaks. You can use `option` + `left arrow`/`right arrow` to look through the string for spaces. When you see them, delete them, but realize that this will also stop on other word separators like `-` and `.` and `_` which should __NOT__ be deleted.

#### The Rest

Leave the `Project Namespace` text field blank. Ensure `RBAC-enabled cluster` is checked, then click the green __Add Kubernetes Cluster__ button:

![cluster config](img/k8s_cluster/k8s_add_token_rbac_save.png)

### Install Tiller and Other Apps into your Cluster

Yes, you already installed Helm Tiller once, but that was into `kube-system` namespace, to enable the installation of GitLab into the cluster. __Normally__, you would not install your applications into the same cluster as your GitLab instance (we're doing that in this lab for convenience). In a normal situation you would attach a new cluster to your project, and then set up Helm Tiller in that new cluster to enable GitLab to deploy and manage the apps in the `gitlab-managed-apps` namespace it will create.

GitLab makes this very easy! All you have to do is click the __Install__ button in the `Helm Tiller` section.

![install tiller](img/k8s_cluster/install_tiller.png)

Wait a few moments for the installation to complete. You will know the install was successful when the __Install__ button stops spinning and changes to `Installed`, and the other application options become available.

<aside>
<h3><strong>Troubleshooting</strong></h3>
<p><strong>401 error code</strong>: If your Tiller installation fails with a `401 error code` - Check that your token copied and pasted ok. 401 means the token was not good. Sometimes copying it out of a web interface/terminal will add newlines and/or spaces. Scroll down on the __GitLab web page__ until you see `Kubernetes cluster details`, expand it, and paste your token into the `Token` field again. Use `option` + `left arrow`/`right arrow` to look through the string for spaces. When you see them, delete them, but realize that this will also stop on other word separators like `-` and `.` and `_` which should __NOT__ be deleted. Once you've removed all the spaces, __Save__ and try to re-install Tiller.
<p>
<p><strong>403 error code</strong>: If your Tiller installation fails with a `403 error code` - Check that the service account the token is for has proper cluster-admin permissions and that you are referencing the service account from the right namespace. These should be correct if you copy and pasted the provided command lines for this.
</aside>

Next, click the __Install__ button for each of the following apps (these can all be installed in parallel):

* Ingress
* Prometheus
* GitLab Runner

![install software](img/k8s_cluster/install_software.png)


#### Verify Installation of Tiller and Other Software

Once the apps are done installing their buttons will all say `Installed` and the `Ingress Endpoint` should be filled in. If the buttons say `Installed` and after a minute there is no value under `Ingress IP Address` then try reloading the page:

![k8s apps installed](img/k8s_cluster/k8s_apps_installed.png)


As a next step, let's go take a closer look to verify that the software was installed into your Kubernetes cluster by running the following in the Cloud Shell:

```bash
kubectl get namespaces
```

In the output, you will notice that there is now a namespace called `gitlab-managed-apps`. See what resources are in that namespace by running:

```bash
kubectl get pods --namespace gitlab-managed-apps
```

You will notice there are pods for the following apps:

* Tiller
* Ingress
* Prometheus
* Gitlab Runner

This means your GKE cluster is successfully connected to GitLab and is ready to be deployed to!

### Configure the domain name associated with your cluster

For Auto DevOps to work you need to assign a valid domain name to you cluster. This domain name should be set to resolve to the external IP address on your just installed Ingress service.

Copy the generated external IP address (it might take a minute for the IP address to show up. If it takes longer than a minute try refreshing the web page):

![k8s ingress IP](img/k8s_cluster/k8s_ingress_IP.png)

Then add it at the top of the cluster page under `Base domain` with ".nip.io" appended to the IP address, and click the __Save Changes__ button. Like earlier, this will use the nip.io dynamic dns service to generate a usable public domain name for your deployed apps:

![k8s add domain](img/k8s_cluster/k8s_add_domain.png)

### About the Auto DevOps configuration

GitLab provides an out-of-the-box, end-to-end automatic CI/CD pipeline called Auto DevOps. Auto DevOps will build, test, secure, deploy, and monitor your apps for you. This is enabled by default and set to deploy to production at the end of the pipeline.

In this image you can see the Auto DevOps default configuration. In this lab you don't need to make any changes to it, so we won't go there:

![Auto DevOps settings](img/configure/autodevops_settings.png)

You will note that by default `Continuous deployment to production` is selected, but that you can also select to have GitLab do `Continuous deployment to production using timed incremental rollout` or `Automatic deployment to staging, manual deployment to production` (which will also do manual incremental rollout).

It is that easy! When you utilize GitLab, with Auto DevOps enabled, the commits to your edits will kick off a pipeline to build, test, secure, deploy, and monitor your app for you.

## Let's Change Some Code

Now go and make a small change to the project you imported so that you can see what GitLab and Google Kubernetes Engine can do together.

### Launch the editor
Start by going to the file repository.

![Go to files](img/create/go_to_files.png)

Do your editing in the built-in web IDE by clicking on the __Web IDE__ button in the upper right of the window. You can of course use CLI git or any other git compatible tooling, but we will keep it simple for today.

![Launch Web IDE](img/create/launch_web_ide.png)

### Edit the code
In the Web IDE, on the left side file tree, navigate down to `HelloController.java` and click on it to view and edit. (`src` -> `main` -> `java` -> `hello` -> `HelloController.java`)

![Navigate to file](img/create/webide_file_nav.png)

Make 4 small edits to this file:

1. Change the background color to `#6b4fbb` by replacing the highlighted text.
   ![File change background color](img/create/file_change_bg.png)

1. Edit the message and put whatever you want (suggestion: replace `Spring Boot` with `GitLab!!`)
   ![File change message](img/create/file_change_message.png)

1. Add a graphic to the message body by pasting the following code after `line 16`:
   ```
   message += "<p><center><img src='https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png' alt='Tanuki' width='250'></center>";
   ```
   ![File change add to message](img/create/file_change_message_add.png)

1. Remove the `TODO` comment line (delete all of line 15)
![File change todo delete](img/create/file_change_todo_delete.png)

### Commit the changes
When you are done editing, your file should look as pictured below. Click on the blue __Commit...__ button.

![Commit changes](img/create/webide_commit.png)

Add a short commit message, click the option to `Create a new branch and merge request` (accept the default branch name), and then click on the green __Stage & Commit__ button.

![Stage and commit changes](img/create/webide_commit_2.png)

GitLab will now lead you to create the Merge Request, which is how it tracks all of the details around your submitted changes. Leave all the default input on the top of the screen.

![Merge request top](img/create/mr_top.png)

Now scroll to the bottom of the page and check the box to have GitLab `Delete source branch when merge request is accepted` so GitLab cleans up after you when you're done. Then click on the green __Submit merge request__ button.

![Merge request top](img/create/mr_submit.png)


## Deliver the changes

GitLab automatically kicks off a pipeline to build your code changes, tests them, and then deploys them to a personal review environment (called a Review App) where you and other stakeholders can validate your changes. Behind the scenes GitLab and Google Kubernetes Engine are orchestrating to not only scale up the runners during the test stages, but also to spin up (and eventually spin down) the review environments.

### Look at the running pipeline

Click on the `pie chart` or the `pipeline ID` in the second row to get a closer look at the pipeline.

![MR with running pipeline](img/deliver/mr_running_pipeline.png)

Looking at the pipeline you can see all the tests (security and otherwise) that GitLab is running in parallel, as well as the review app setup and further testing. This is all out of the box behavior available with almost no configuration.

![pipeline graph](img/deliver/pipeline_graph.png)

The pipeline will take several minutes to complete. Once a job is running you can click into it to see live output and get more information about it. Go ahead and look into the details of a job.

![goto job details](img/deliver/goto_job_details.png)

Click on the `!1` link to bring you back to the merge request that this pipeline was launched from.

![job details](img/deliver/job_details.png)

### Review your changes

Once back at the Merge Request click on the __View app__ button to take a look at the running code changes.

![MR ready to merge](img/deliver/mr_ready_to_merge.png)

<aside>
If the __View app__ button isn't there, then the pipeline is probably still running and hasn't completed the Review App job yet. Explore around some more then come back once it is finished.
</aside>

Remember, this is your code change running from your branch. Nothing has been merged into the main branch yet. Once you have confirmed that your code changes resulted in the changes you wanted, you can close this window/tab and go back to the Merge Request.

![Review app](img/deliver/review_app.png)

### Merge the changes and send them to production

Once back on the merge request, you can look at the other test results, and decide if you want to merge the changes. If you do, and the pipeline has completed, click on the green __Merge__ button.

![Merge it!](img/deliver/mr_merge_it.png)


#### OR
If the pipeline is still running, the merge button will be blue and will offer to __Merge when pipeline succeeds__. If you trust your pipeline and want to send your changes to production without reviewing the results, then click this button.

![merge when pipeline succeeds](img/deliver/merge_when_pipeline_succeeds.png)

Either way, GitLab will complete the merge, fire off another pipeline to deliver the changes to production, and then clean up the completed branches and Kubernetes resources.

<p align="center">
<h2>Congratulations!<br>
You're delivering to production!</h2>
<img src="img/deliver/birthday-tanuki.png" alt="celebration GitLab logo">
</p>

### Optional step: See your change in Production

If you've completed the lab in time for the second pipeline to complete, then your app should be deployed into your production environment. To see it running in production go to the __Environments__ page:

![go to environments page](img/deliver/go_to_environments.png)

This will take you to the page where you can manage all of your environments. On this screen, you can see and get information about each of the Kubernetes pods running your application (including the pod logs), you can go see the built-in monitoring data that GitLab is automatically configured to collect about your app, you can see the history of deployments. . .

![environments page](img/deliver/environments.png)

. . . and you can see your running app (by clicking on the icon the red arrow is pointing to):

![production app](img/deliver/production_app.png)

## Next steps /learn more

The best of all is that we have only scratched the surface here, but hopefully, this provides you a solid
base to explore GitLab and Google Kubernetes Engine further.

### Explore more of GitLab

Sign up for a [free 30-day trial](http://bit.ly/gitlab_free_trial) (SaaS or self-managed).

GitLab is a single application for the entire software development life cycle. It offers
functionality from planning to creation, testing to packaging, deploying,
securing, and monitoring, allowing you to execute your entire workflow within
it's single interface. GitLab runs basically anywhere you want, from on-premise
bare metal all the way up to managed Kubernetes offerings.

### Finish your Quest

<!-- ![Kubernetes-Badge](img/migrate/kubernetes-badge.png)

![Web-Badge](img/migrate/sites-badge.png) -->

This self-paced lab is part of the Qwiklabs [Kubernetes Solutions](https://google.qwiklabs.com/quests/45) and [Website and Web Applications](https://google.qwiklabs.com/quests/39) Quests. A Quest is a series of related labs that form a learning path. Completing this Quest earns you the badge above, to recognize your achievement. You can make your badge (or badges) public and link to them in your online resume or social media account. Enroll in a Quest and get immediate completion credit if you've taken this lab. See [other available Qwiklabs Quests](https://google.qwiklabs.com/catalog).

### Take your next lab

Learn more about GKE with [NGINX Ingress Controller on Google Kubernetes Engine](https://google.qwiklabs.com/catalog\_lab/910) or [Running Dedicated Game Servers in Google Kubernetes Engine](https://google.qwiklabs.com/catalog\_lab/764).

Learn more about Kubernetes by taking a Quest. For the advanced user, try [Kubernetes in the Google Cloud](https://google.qwiklabs.com/quests/29). For the expert user, try [Kubernetes Solutions](https://google.qwiklabs.com/quests/45).

### Resources

Google GKE  
* [Google GKE Overview](https://cloud.google.com/kubernetes-engine/)  
* [Kubernetes Engine Documentation](https://cloud.google.com/kubernetes-engine/docs/)  
* [Production-Grade Container Orchestration](https://kubernetes.io/)  
* [Kubernetes The Easy Way! (For Developers In 2018)](https://youtu.be/kOa\_llowQ1c)  

Gitlab  
* [Get a free 30 day GitLab Ultimate trial license](https://about.gitlab.com/free-trial/)  
* [GitLab website](https://about.gitlab.com/)  
* [GitLab Auto DevOps Docs](https://docs.gitlab.com/ee/topics/autodevops/)  
* [Docs on Customizing Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/#customizing)  

![[/fragments/TrainingCertificationOverview]]

##### Manual Last Updated April 4, 2019
##### Lab Last Tested April 3, 2019
