#!/bin/bash

# randomaize the zone
python -c 'import random, os; \
l = random.choice(["a", "b", "c", "f"]); \
os.system("gcloud config set compute/zone us-central1-{}".format(l)); \
print("You will be using GCP zone: us-central1-{}".format(l))'

# create the cluster
gcloud container clusters create gitlab-gke-cl --num-nodes=3 --machine-type=n1-standard-2 --node-version=1.11.7-gke.4

# authenticate to the cluster
gcloud container clusters get-credentials gitlab-gke-cl

# install helm
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash

# create service account
kubectl create serviceaccount -n kube-system tiller

# bind service account to cluster-admin
kubectl create clusterrolebinding tiller-binding \
   --clusterrole=cluster-admin \
   --serviceaccount kube-system:tiller
   
# init helm
helm init --service-account tiller

# setup helm with gitlab repo
helm repo add gitlab https://charts.gitlab.io/
helm repo update

# create external IP
gcloud compute addresses create endpoints-ip --region us-central1

# store I in variable
export LB_IP_ADDR="$(gcloud compute addresses list | awk '/endpoints-ip/ {print$2}')"; echo $LB_IP_ADDR

# set email address for Lets encrypt
export EMAIL=<YOUR_EMAIL_ADDRESS>

# deploy GitLab to cluster
helm upgrade --install gitlab gitlab/gitlab                    \
             --timeout 600                                     \
             --set global.hosts.externalIP=${LB_IP_ADDR}       \
             --set global.hosts.domain=${LB_IP_ADDR}.nip.io    \
             --set certmanager-issuer.email=${EMAIL}           \
             --set gitlab-runner.runners.privileged=true

